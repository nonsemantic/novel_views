function frac = filterPatchList(posPatches)
% Author: dfouhey@cs.cmu.edu (David Fouhey), wajih.1234@gmail.com (Wajahat Hussain)
    masks = [];
%     tic
    for i=1:numel(posPatches)
%         fprintf('Precaching masks %d/%d\n',i,numel(posPatches));
        imName = posPatches(i).im;
        [fpath name ext] = fileparts(imName);
        maskName = [strrep(fpath,'Images','masks') '/' name '.png'];
        
          mask = imread(maskName);
        if ~numel(masks)
            masks = containers.Map(maskName,mask);
        else
            if ~isKey(masks,maskName)
                masks(maskName) = mask;
            end
        end
    end
%     timecnt = toc;
%     fprintf('container time %d \n',timecnt)

    frac = zeros(numel(posPatches),1);
    for i=1:numel(posPatches)
        minX = min(posPatches(i).x1,posPatches(i).x2);
        minY = min(posPatches(i).y1,posPatches(i).y2);
        maxX = max(posPatches(i).x1,posPatches(i).x2);
        maxY = max(posPatches(i).y1,posPatches(i).y2);
        imName = posPatches(i).im;
        [fpath name ext] = fileparts(imName);
        maskName = [strrep(fpath,'Images','masks') '/' name '.png'];
        mask = masks(maskName);
        try
            patch = mask(minY:maxY,minX:maxX)/255;
            frac(i) = mean(patch(:));
        catch
            frac(i) = 0;
        end
    end

end

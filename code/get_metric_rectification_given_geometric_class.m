function [Hopt,Hheu,doneflag,fx2,Hopt2,cost2]=get_metric_rectification_given_geometric_class(seg_bndy,vps,I,f_flag,cx,cy,fx,vpmod,gclass)

% This function provides metric rectification.
% wajahat 06-14

Hopt = []; % metric rectification homopgraphy
Hheu=[]; 
doneflag=0; % if 1 than metric rectification done, 0 otherwise
fx2 = []; % optimized focal length. Can be different to f
gamma = 0; 
Hopt2 = []; % This is similar to Hopt with the addiontal contraint that vertial walls must be vertical
cost2 = []; % If value low than Hopt2 done. 

% get heuristic homography
maxsize = 300;
%  Hheu = get_heuristic_homo(seg_bndy,[vps(1,:);vps(2,:)],I,0);
% %  [newim, newT] = imTrans(I, Hheu, [seg_bndy(3) seg_bndy(4) seg_bndy(1) seg_bndy(2)],maxsize);
 
 
% transform patch orientation to lines
numl =5;
[line_x,line_y] = region_to_ortholines(seg_bndy,[vps(1,:);vps(2,:)],numl,I,0);
 
 
% order line end pts. pt1 closer to vp than pt2
[line_x,line_y]=order_line_endpts(line_x,line_y,[vps(1,:);vps(2,:)],I,0);
 
% transform lines in to implicit form i.e. ax+by+c=0;
numlx = size(line_x,1);
line_xc = [];
for ll=1:numlx
    dummy_normal = cross([line_x(ll,[1 3]) 1],[line_x(ll,[2 4]) 1]); % ax+by+c=0 
    line_xc = [line_xc; dummy_normal];
end

numly = size(line_y,1);
line_yc = [];
for ll=1:numly
    dummy_normal = cross([line_y(ll,[1 3]) 1],[line_y(ll,[2 4]) 1]);
    line_yc = [line_yc; dummy_normal];
end
 
% set rectification homogrpahy opt parameters
options=optimset( 'Display', 'off', 'MaxIter', 2000, 'TolFun', 10^-200, 'TolX', 10^-10,'MaxFunEvals',10^20, 'LargeScale', 'off');
 
 
maxitr = 51; doneflag =0;
 
% random initial seed
initial_seed = 2*pi*rand(maxitr,2);
initial_seed = [0 0; initial_seed];

cnt = 0; 
 while(cnt < maxitr && ~doneflag )
     
     cnt = cnt +1;
     
     % homography optimization
     if f_flag
   
         [homo_param, cost] = ...
                fminunc(@(t)(metric_homography_opt(t,line_xc,line_yc,fx,cx,cy)),[initial_seed(cnt,1), initial_seed(cnt,2)], options);
         fx2 = fx;
     else
         [homo_param, cost] = ...
                fminunc(@(t)(metric_homography_opt2(t,line_xc,line_yc,cx,cy)),[initial_seed(cnt,1), initial_seed(cnt,2),fx], options);

         fprintf('gt focal len =  %d, homography focal len = %d \n', fx, homo_param(3))
         fx2 = homo_param(3);
     end
     
     % get metric homography
     alpha = homo_param(1);
     beta  = homo_param(2);
    
     Hopt = get_homography_given_param(alpha,beta,fx2,cx,cy,gamma,[]);
     Hinv = inv(Hopt)';
      
     
    % check angle b/w lines after homography application (if orthogonal then done)
    ang_before = [];
    ang_after = [];

    for ll=1:numlx
        for mm=1:numly
       
            % line param ax+by+c=0
            l1 = line_xc(ll,:);
            l2 = line_yc(mm,:);
       
            % line unit normals
            l1n = l1(1:2)./norm(l1(1:2)); %norm(l1n)
            l2n = l2(1:2)./norm(l2(1:2)); %norm(l2n)
       
            % apply rectifying homography to lines
            l1t = (Hinv*l1')';
            l2t = (Hinv*l2')';
       
            % line unit normals
            l1tn = l1t(1:2)./norm(l1t(1:2));% norm(l1tn)
            l2tn = l2t(1:2)./norm(l2t(1:2));% norm(l2tn)
       
            % get angle between lines
            ang_before = [ang_before acosd(l1n*l2n')];
            ang_after = [ang_after acosd(l1tn*l2tn')];
        end
    end
    
    angdiff = mean(abs(90-ang_after));
    
    
    
    %
    if angdiff < 5
        doneflag = 1;
    end
     
     
 end
 
 if doneflag==0 % metric rectification not achieved
     return;
 end
 
% % get inplane rotation
% gamma = get_inplane_rotation(Hopt,l1(1,:));
 
% % get in plane rotation and reflection
[Hopt2,cost2]=get_inplane_rotation_and_reflection(Hopt,line_x,line_y,vpmod,gclass);



 return;
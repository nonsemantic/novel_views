function vpcase =  vpmode(vp)


% Input: ordered 3 vps i.e., [vertical vp;horizontal vp; in image vp]

vpcase =[];

if vp(1,2) > 0 && vp(2,1)>0
    vpcase=1;
elseif vp(1,2) > 0 && vp(2,1)<0
    vpcase=2;
elseif vp(1,2) < 0 && vp(2,1)<0
    vpcase=3;
elseif vp(1,2) < 0 && vp(2,1)>0
    vpcase=4;    
end

return;
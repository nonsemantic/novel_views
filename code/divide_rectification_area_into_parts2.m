% This function further divides the region on one side of the horizon into
% sub regions. It help to have resonable resolution for regions away from
% horizon line.

function bndy_new = divide_rectification_area_into_parts2(seg_bndy,faceindx,numparts,overlap,I,Do_Display)

bndy_new = zeros(numparts,4);


if faceindx ==1 % left
    minx = seg_bndy(1); maxx = seg_bndy(2);
    regjump = (maxx-minx)/numparts;
%     regbndy = minx:regjump:maxx;
%     pixoverlap = regjump*(overlap/2);
    for pp=1:numparts
%         if pp==1
%             bndy_new(pp,:) = [seg_bndy(1) regbndy(2)+pixoverlap seg_bndy(3:4)];
              bndy_new(pp,:) = [seg_bndy(1) seg_bndy(2)-((pp-1)*regjump) seg_bndy(3:4)];
%         elseif pp==numparts
%             bndy_new(pp,:) = [regbndy(pp)-pixoverlap seg_bndy(2:4)];
%         else
%             bndy_new(pp,:) = [regbndy(pp)-pixoverlap regbndy(pp+1)+pixoverlap  seg_bndy(3:4)];
%         end
    end
elseif faceindx ==2 % right
    minx = seg_bndy(1); maxx = seg_bndy(2);
    regjump = (maxx-minx)/numparts;
%     regbndy = minx:regjump:maxx;
%     pixoverlap = regjump*(overlap/2);
    for pp=1:numparts
%         if pp==1
%             bndy_new(pp,:) = [seg_bndy(1) regbndy(2)+pixoverlap seg_bndy(3:4)];
              bndy_new(pp,:) = [seg_bndy(1)+((pp-1)*regjump) seg_bndy(2) seg_bndy(3:4)];
%         elseif pp==numparts
%             bndy_new(pp,:) = [regbndy(pp)-pixoverlap seg_bndy(2:4)];
%         else
%             bndy_new(pp,:) = [regbndy(pp)-pixoverlap regbndy(pp+1)+pixoverlap  seg_bndy(3:4)];
%         end
    end 
    
elseif faceindx ==3 % bottom
    miny = seg_bndy(3); maxy = seg_bndy(4);
    regjump = (maxy-miny)/numparts;
%     regbndy = miny:regjump:maxy;
%     pixoverlap = regjump*(overlap/2);
    for pp=1:numparts
%         if pp==1
%             bndy_new(pp,:) = [seg_bndy(1:2) seg_bndy(3) regbndy(2)+pixoverlap ];
              bndy_new(pp,:) = [seg_bndy(1:2) seg_bndy(3)+((pp-1)*regjump) seg_bndy(4) ];
%         elseif pp==numparts
%             bndy_new(pp,:) = [seg_bndy(1:2) regbndy(pp)-pixoverlap seg_bndy(4)];
%         else
%             bndy_new(pp,:) = [seg_bndy(1:2) regbndy(pp)-pixoverlap regbndy(pp+1)+pixoverlap];
%         end
    end 
    
    
elseif faceindx ==4 % top
    miny = seg_bndy(3); maxy = seg_bndy(4);
    regjump = (maxy-miny)/numparts;
%     regbndy = miny:regjump:maxy;
%     pixoverlap = regjump*(overlap/2);
    for pp=1:numparts
%         if pp==1
%             bndy_new(pp,:) = [seg_bndy(1:2) seg_bndy(3) regbndy(2)+pixoverlap ];
              bndy_new(pp,:) = [seg_bndy(1:2) seg_bndy(3) seg_bndy(4)-((pp-1)*regjump) ];
%         elseif pp==numparts
%             bndy_new(pp,:) = [seg_bndy(1:2) regbndy(pp)-pixoverlap seg_bndy(4)];
%         else
%             bndy_new(pp,:) = [seg_bndy(1:2) regbndy(pp)-pixoverlap regbndy(pp+1)+pixoverlap];
%         end
    end 
    
else % center
    
    minx = seg_bndy(1); maxx = seg_bndy(2);
    regjumpx = (maxx-minx)/(2*numparts);
    pixoverlapx = regjumpx*(overlap/2);
    
    miny = seg_bndy(3); maxy = seg_bndy(4);
    regjumpy = (maxy-miny)/(2*numparts);
    pixoverlapy = regjumpy*(overlap/2);
    
    
    
    for pp=1:numparts
        if pp==1
            bndy_new(pp,:) = [seg_bndy(1)+((pp-1)*regjumpx) seg_bndy(2)-((pp-1)*regjumpx) ...
                              seg_bndy(3)+((pp-1)*regjumpy) seg_bndy(4)-((pp-1)*regjumpy)];
        else
            bndy_new(pp,:) = [seg_bndy(1)+((pp-1)*regjumpx)-pixoverlapx seg_bndy(2)-((pp-1)*regjumpx)+pixoverlapx ...
                              seg_bndy(3)+((pp-1)*regjumpy)-pixoverlapy seg_bndy(4)-((pp-1)*regjumpy)+pixoverlapy];
        end  
    end
    
end

if Do_Display
    h=figure;
    col={'r','c','g','b','y'};
    for pp=1:numparts
        subplot(1,numparts,pp)
        imshow(I);
        hold on;
        plot(bndy_new(pp,[1 2 2 1 1]),bndy_new(pp,[3 3 4 4 3]),col{pp},'LineWidth',4);
     end
    close(h);
end
return

% This function rectifies the given image into five regions, i.e., left,
% right, bottom, top, center. These parts are wrt to the horizon.

function complete_metric_rectification_multiparts(src,imgname,datadir,savedir,maskdir,homodir)


f_flag = 1; % focal length fixed or part of optimization. 1 fixed, 0 in opt
save_img = 1; % save 1, no 0
save_rotimg = 0; % save rotated version of bottom region [90 180 270]
rot_ang = [90 180 270];
maxsize = 1200;%2600;%400;


parts_flag =1; % 0 if one part on each side of the horizon
numparts = 2; % if parts_flag =1 , num of parts on side of horizon
partmode =1; % 1: overlap sub parts, 2: whole to small part
overlap = 0.80; %0.75 % if parts_flag =1, common area between two neighbouring parts

%% get image
I= imread([src '/' imgname]);
[imrows,imcols,imdims]=size(I);
cx = imcols/2;
cy = imrows/2;


%% get vanishing points file
vpfile=fullfile(datadir,[imgname(1:end-4) '_vp.mat']);
if exist(vpfile,'file')
    load(vpfile);
    if exist('vp_new','var')
        clear vp;
        vp = vp_new;
    end
else
    fprintf('Computing vanishing points  \n');
    return;
    minlen = 10;
    lenjump =  10;
    imresolution = 600; 
      
    doneflag = 0; cnt = 0;
    while ~doneflag
        try
            currlen = minlen + cnt*lenjump;
            [vp p All_lines]=getVP3([src '/'],imgname,1,datadir,currlen,imresolution);
            doneflag = 1;
        catch
            fprintf('Done try %d out of %d \n',cnt,5)
            if cnt == 5
               doneflag =1;
               fprintf('Unable to calculate vp \n');
               return;
            end
            cnt = cnt +1;
        end
    end
end

VP=vp;
if numel(VP)<6
    fprintf('Bad VP \n');
    return;
end
vp=[VP(1) VP(2);VP(3) VP(4);VP(5) VP(6)];
[vp,P]=ordervp(vp,imrows,imcols,p);
[vv linemem]=max(P,[],2);

% get vp case (1:4)
vpmod = vpmode(vp);


%% get the focal length
vps = {[vp(1,1), vp(1,2)],[vp(2,1),vp(2,2)],[vp(3,1),vp(3,2)]};
[vps, fx]=calibrate_cam_fouhey(vps,imrows,imcols);


%% get part wrt horizon
[part_region, vispart] =  get_parts_wrt_horizon(vp,vpmod,I,0);

geom_classes = {'l','r','b','t','c'};
save_names = {'left','right','bottom','top','center'};

vpindx = [1 3; ...
          1 3; ...
          2 3; ...
          2 3; ...
          1 2];

% get rectification
for pp=1:5
    

    if ~vispart(pp) ||  pp==4 || (pp==3  && ~(strcmp(imgname,'01_14.jpg') || strcmp(imgname,'17_07.jpg')|| strcmp(imgname,'16_13.jpg')))
        continue;
    end
    visflag = 1;
     if pp==5
        numparts = 2;
    end
    
   % metric rectifcation homography
   seg_bndy = part_region(pp,:);
   vp1=vp(vpindx(pp,1),:);
   vp2=vp(vpindx(pp,2),:);
   doneflag = 0;
   while ~doneflag
   
        [Hopt,Hheu,doneflag,fx2,Hopt2,cost2]=get_metric_rectification_given_geometric_class(seg_bndy,[vp1;vp2],I,f_flag,cx,cy,fx,vpmod,geom_classes{pp});
   
        if doneflag==0
%             fprintf('Could not rectify %s \n',geom_classes{pp})
%             continue;
           [seg_bndy, visflag]=  delete_area_given_rect_failure(seg_bndy,geom_classes{pp});
           
           if visflag==0
               break;
           end
           
        end
        
        
   end
   
   if visflag==0
       fprintf('Could not rectify even after deleting %s \n',geom_classes{pp})
       vispart(pp)=visflag;
       part_region(pp,:)=[0 0 0 0];
       continue;
   end
   
   
   % avoid img area near the vanishing line.
   [seg_bndy, visflag]=delete_area_near_vanishing_line(Hopt2,seg_bndy,vpmod,geom_classes{pp},I,0);
   
   
   
   if visflag==0
       vispart(pp)=visflag;
       part_region(pp,:)=[0 0 0 0];
       continue;
   end
   
   % get image regions to be rectified separatly
   if parts_flag
       if partmode==1
            seg_bndy = divide_rectification_area_into_parts(seg_bndy,pp,numparts,overlap,I,0);
       elseif partmode==2
            seg_bndy = divide_rectification_area_into_parts2(seg_bndy,pp,numparts,overlap,I,0);
       end
   end
   
   numregs = size(seg_bndy,1);
   
   for nn=1:numregs
       
        % get rectified image
%         maxsize = 2600;%400;
        Homo_rect = Hopt2;
        [newim, newT] = imTrans(I,Homo_rect, [seg_bndy(nn,3) seg_bndy(nn,4) seg_bndy(nn,1) seg_bndy(nn,2)],maxsize);
        
        % get rectified mask
        [newmask, ~] = imTrans(ones(imrows,imcols),Hopt2, [seg_bndy(nn,3) seg_bndy(nn,4) seg_bndy(nn,1) seg_bndy(nn,2)],maxsize);  


        % save
        if save_img
            if pp==3 || pp==4 % floor & ceiling can be rotated to achieve rotational invariance. Not needed for walls.
                
                % save image
                savename =  fullfile(savedir,[imgname(1:end-4) '_' save_names{pp} '_' num2str(nn) '_0.jpg']);
                imwrite(newim,savename,'jpg');
                
                % save mask
                savename =  fullfile(maskdir,[imgname(1:end-4) '_' save_names{pp} '_' num2str(nn) '_0.png']);
                imwrite(newmask,savename,'jpg');
                
                % save homography
                savename =  fullfile(homodir,[imgname(1:end-4) '_' save_names{pp} '_' num2str(nn) '_0.mat']);
                save(savename,'newT','Homo_rect');
                
                
                if save_rotimg 
                    for rr=1:3
                        
                        rang = rot_ang(rr);
%                                               
                        % get roation matrix
                        Rz = [ cosd(rang)   -sind(rang)      0; ...
                               sind(rang)    cosd(rang)      0; ...
                               0               0              1]; 
                        % rotate image
                        Homo_rect = Rz*Hopt2;
                        [newim2, newT] = imTrans(I,Homo_rect, [seg_bndy(nn,3) seg_bndy(nn,4) seg_bndy(nn,1) seg_bndy(nn,2)],maxsize);

                        % save rotated image
                        savename =  fullfile(savedir,[imgname(1:end-4) '_' save_names{pp} '_' num2str(nn) '_' num2str(rang) '.jpg']);
                        imwrite(newim2,savename,'jpg');
                    
                        % save rotated mask
                        [newmask, ~] = imTrans(ones(imrows,imcols),Homo_rect, [seg_bndy(nn,3) seg_bndy(nn,4) seg_bndy(nn,1) seg_bndy(nn,2)],maxsize);
                        savename =  fullfile(maskdir,[imgname(1:end-4) '_' save_names{pp} '_' num2str(nn) '_' num2str(rang) '.png']);
                        imwrite(newmask,savename,'jpg');


                        % % save homography
                        % pt2 = inv(Homo_rect)*newT*pt1
                        % pt2 is in unrectified image
                        % pt1 in rectified image
                        savename =  fullfile(homodir,[imgname(1:end-4) '_' save_names{pp} '_' num2str(nn) '_' num2str(rang) '.mat']);
                        save(savename,'newT','Homo_rect'); 
                    end
                end
            else
                
                % save image
                savename =  fullfile(savedir,[imgname(1:end-4) '_' save_names{pp} '_' num2str(nn) '.jpg']);
                imwrite(newim,savename,'jpg');
            
                % save mask
                savename =  fullfile(maskdir,[imgname(1:end-4) '_' save_names{pp} '_' num2str(nn) '.png']);
                imwrite(newmask,savename,'jpg');
                
               % save homography
                savename =  fullfile(homodir,[imgname(1:end-4) '_' save_names{pp} '_' num2str(nn) '.mat']);
                save(savename,'newT','Homo_rect');
                
            end
        end
   end
end



close all;



























return
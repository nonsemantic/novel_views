% This function rectifies the given image into five regions, i.e., left,
% right, bottom, top, center. These parts are wrt to the horizon.
% Author: wajahat hussain, 5 july 2014.

function complete_metric_rectification(src,imgname,datadir,savedir)


f_flag = 1; % focal length fixed or part of optimization. 1 fixed, 0 in opt
save_img = 1; % save 1, no 0
save_rotimg = 0; % save rotated version of bottom region [90 180 270]
rot_ang = [90 180 270];

%% get image
I= imread([src '/' imgname]);
[imrows,imcols,imdims]=size(I);
cx = imcols/2;
cy = imrows/2;


%% get vanishing points file
vpfile=fullfile(datadir,[imgname(1:end-4) '_vp.mat']);
if exist(vpfile,'file')
    load(vpfile);
else
    
    fprintf('Computing vanishing points  \n');
    
    try
            [vp p All_lines]=getVP(src,imgname,1,datadir);
    catch
            [vp p All_lines]=getVP2(src,imgname,1,datadir,200); % incase memory error 
    end
end

VP=vp;
if numel(VP)<6
    return;
end
vp=[VP(1) VP(2);VP(3) VP(4);VP(5) VP(6)];
[vp,P]=ordervp(vp,imrows,imcols,p);
[vv linemem]=max(P,[],2);

% get vp case (1:4)
vpmod = vpmode(vp);


%% get the focal length
vps = {[vp(1,1), vp(1,2)],[vp(2,1),vp(2,2)],[vp(3,1),vp(3,2)]};
[vps, fx]=calibrate_cam_fouhey(vps,imrows,imcols);


%% get part wrt horizon
[part_region, vispart] =  get_parts_wrt_horizon(vp,vpmod,I,0);

geom_classes = {'l','r','b','t','c'};
save_names = {'left','right','bottom','top','center'};

vpindx = [1 3; ...
          1 3; ...
          2 3; ...
          2 3; ...
          1 2];

% get rectification
for pp=1:5
    

    if ~vispart(pp) %|| pp==4
        continue;
    end
    
   % metric rectifcation homography
   seg_bndy = part_region(pp,:);
   vp1=vp(vpindx(pp,1),:);
   vp2=vp(vpindx(pp,2),:);
   [Hopt,Hheu,doneflag,fx2,Hopt2,cost2]=get_metric_rectification_given_geometric_class(seg_bndy,[vp1;vp2],I,f_flag,cx,cy,fx,vpmod,geom_classes{pp});
   
   if doneflag==0
       continue;
   end
   
   % avoid img area near the vanishing line.
   [seg_bndy, visflag]=delete_area_near_vanishing_line(Hopt2,seg_bndy,vpmod,geom_classes{pp},I,0);
    
   if visflag==0
       vispart(pp)=visflag;
       part_region(pp,:)=[0 0 0 0];
       continue;
   else
       part_region(pp,:)=seg_bndy;
   end
   
   % get rectified image
   maxsize = 3600;%600;
   [newim, newT] = imTrans(I,Hopt2, [seg_bndy(3) seg_bndy(4) seg_bndy(1) seg_bndy(2)],maxsize);
%    h = figure;
%    imshow(newim);
%    close(h);
   
   % save
   if save_img
        if pp==3 || pp==4
            
            savename =  fullfile(savedir,[imgname(1:end-4) '_' save_names{pp} '_0.jpg']);
            imwrite(newim,savename,'jpg');
            if save_rotimg 
                for rr=1:3
                    rang = rot_ang(rr);
                    newim2 = imrotate(newim,rang);
                    savename =  fullfile(savedir,[imgname(1:end-4) '_' save_names{pp} '_' num2str(rang) '.jpg']);
                    imwrite(newim2,savename,'jpg');
                end
            end
        else
            savename =  fullfile(savedir,[imgname(1:end-4) '_' save_names{pp} '.jpg']);
            imwrite(newim,savename,'jpg');
        end
   end
end































return
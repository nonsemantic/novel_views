function H = get_homography_given_param(alpha,beta,f,cx,cy,gamma,scale)

H = [];

if isempty(gamma)
    gamma=0;
end

if isempty(scale)
    scale=1;
end



K = [f 0 cx; ...
     0 f cy; ...
     0 0  1];

% 3d rotation matirces 
Rx = [1        0                   0; ...
      0     cos(alpha)   -sin(alpha); ...
      0     sin(alpha)    cos(alpha)];
 
Ry = [ cos(beta)      0     sin(beta); ...
          0           1             0; ... 
      -sin(beta)      0     cos(beta)];

Rz = [ cos(gamma)   -sin(gamma)      0; ...
       sin(gamma)    cos(gamma)      0; ...
         0            0              1];

simitrans = [scale   0   0; ...
             0    scale  0; ...
             0       0   1];
 
 H = simitrans*Rz*Ry*Rx*inv(K);
 
 return
         
         
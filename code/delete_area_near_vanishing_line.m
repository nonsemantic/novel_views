function [seg_bndy2,visflag] = delete_area_near_vanishing_line(Hopt2,seg_bndy,vpmod,gclass,I,Do_Display)

% This function deletes the area of the region near the vanishing line
% using heuristics.

% Input: Hopt2: 3x3 metric rectification homography
%        seg_bndy [minx maxx miny maxy]
%        vpmod 1out of 4 possible vanishing point configuration
%        gclass name of the region wrt horizon i.e. l(left), r(right),
%        b(bottom), t(top), c(center)
%        I image

seg_bndy2 =[];
visflag = 0;
% thresholds = [0.40,0.40,0.25,0.25,0.40]; % used for all indoor experiments in 2014

% thresholds = [0.20,0.20,0.25,0.25,0.25];
% thresholds = [0.20,0.20,0.25,0.25,0.20];
% thresholds = [0.10,0.10,0.10,0.10,0.10];
thresholds = [0.20,0.20,0.20,0.20,0.40]; % value for hotel dataset 2015 % th = 0.20; % >= acceptable ratio of two sides of the region after homography application
%                                                                       % In case one side is close to infinity line, the this ratio will be very small i.e. < 0.1
pixjump = 10;

[imrows,imcols,imdims]=size(I);


seg_bndy2 = seg_bndy;          
if strcmp(gclass,'l')
    th = thresholds(1);
    
    done = 0;
    while ~done
        
        % side 1 end pts
        pt1 =[seg_bndy2(1) seg_bndy2(3) 1];
        pt2 =[seg_bndy2(1) seg_bndy2(4) 1];
        
        % side 2 end pts
        pt3 =[seg_bndy2(2) seg_bndy2(3) 1];
        pt4 =[seg_bndy2(2) seg_bndy2(4) 1];
        
        % transform points
        ptst =  Hopt2*[pt1',pt2',pt3',pt4'];
        ptst(1,:) = ptst(1,:)./ptst(3,:);
        ptst(2,:) = ptst(2,:)./ptst(3,:);
        
        % get side lengths after transformation
        len1 = sqrt(sum((ptst([1 2],1)-ptst([1 2],2)).^2));
        len2 = sqrt(sum((ptst([1 2],3)-ptst([1 2],4)).^2));
        if len1 > len2
            sideratio = len2/len1;
        else
            sideratio = len1/len2;
        end
        
        % check if the ratio is reasonable
        if sideratio>th
            done=1;
            visflag=1;
        else
            
            % update region
            seg_bndy2(2) = max(seg_bndy2(2)-pixjump,1);
            
            % break if empty
            if seg_bndy2(2)==1
                seg_bndy2 =[];
                break;
            end    
        end
    
    end
    
    
elseif strcmp(gclass,'r')
    
    th = thresholds(2);
    
    done = 0;
    while ~done
        
        % side 1 end pts
        pt1 =[seg_bndy2(1) seg_bndy2(3) 1];
        pt2 =[seg_bndy2(1) seg_bndy2(4) 1];
        
        % side 2 end pts
        pt3 =[seg_bndy2(2) seg_bndy2(3) 1];
        pt4 =[seg_bndy2(2) seg_bndy2(4) 1];
        
        % transform points
        ptst =  Hopt2*[pt1',pt2',pt3',pt4'];
        ptst(1,:) = ptst(1,:)./ptst(3,:);
        ptst(2,:) = ptst(2,:)./ptst(3,:);
        
        % get side ratios after transformation
        len1 = sqrt(sum((ptst([1 2],1)-ptst([1 2],2)).^2));
        len2 = sqrt(sum((ptst([1 2],3)-ptst([1 2],4)).^2));
        if len1 > len2
            sideratio = len2/len1;
        else
            sideratio = len1/len2;
        end
        
        % check if the ratio is reasonable
        if sideratio>th
            done=1;
            visflag=1;
        else
            
            % update region
            seg_bndy2(1) = min(seg_bndy2(1)+pixjump,imcols);
            
            % break if empty
            if seg_bndy2(1)==imcols
                seg_bndy2 =[];
                break;
            end    
        end
    
    end
    
elseif strcmp(gclass,'b')
    
    th = thresholds(3);
    
    done = 0;
    while ~done
        
        % side 1 end pts
        pt1 =[seg_bndy2(1) seg_bndy2(3) 1];
        pt2 =[seg_bndy2(2) seg_bndy2(3) 1];
        
        % side 2 end pts
        pt3 =[seg_bndy2(1) seg_bndy2(4) 1];
        pt4 =[seg_bndy2(2) seg_bndy2(4) 1];
        
        % transform points
        ptst =  Hopt2*[pt1',pt2',pt3',pt4'];
        ptst(1,:) = ptst(1,:)./ptst(3,:);
        ptst(2,:) = ptst(2,:)./ptst(3,:);
        
        % get side ratios after transformation
        len1 = sqrt(sum((ptst([1 2],1)-ptst([1 2],2)).^2));
        len2 = sqrt(sum((ptst([1 2],3)-ptst([1 2],4)).^2));
        if len1 > len2
            sideratio = len2/len1;
        else
            sideratio = len1/len2;
        end
        
        % check if the ratio is reasonable
        if sideratio>th
            done=1;
            visflag=1;
        else
            
            % update region
            seg_bndy2(3) = min(seg_bndy2(3)+pixjump,imrows);
            
            % break if empty
            if seg_bndy2(3)==imrows
                seg_bndy2 =[];
                break;
            end    
        end
    
    end 
    
elseif strcmp(gclass,'t')
    
    th = thresholds(4);
    
    done = 0;
    while ~done
        
        % side 1 end pts
        pt1 =[seg_bndy2(1) seg_bndy2(3) 1];
        pt2 =[seg_bndy2(2) seg_bndy2(3) 1];
        
        % side 2 end pts
        pt3 =[seg_bndy2(1) seg_bndy2(4) 1];
        pt4 =[seg_bndy2(2) seg_bndy2(4) 1];
        
        % transform points
        ptst =  Hopt2*[pt1',pt2',pt3',pt4'];
        ptst(1,:) = ptst(1,:)./ptst(3,:);
        ptst(2,:) = ptst(2,:)./ptst(3,:);
        
        % get side ratios after transformation
        len1 = sqrt(sum((ptst([1 2],1)-ptst([1 2],2)).^2));
        len2 = sqrt(sum((ptst([1 2],3)-ptst([1 2],4)).^2));
        if len1 > len2
            sideratio = len2/len1;
        else
            sideratio = len1/len2;
        end
        
        % check if the ratio is reasonable
        if sideratio>th
            done=1;
            visflag=1;
        else
            
            % update region
            seg_bndy2(4) = max(seg_bndy2(4)-pixjump,1);
            
            % break if empty
            if seg_bndy2(4)==1
                seg_bndy2 =[];
                break;
            end    
        end
    
    end 
       
else
    
    th = thresholds(5);
    
    if vpmod==1 || vpmod==4
        
        done = 0;
        while ~done
        
            % side 1 end pts
            pt1 =[seg_bndy2(1) seg_bndy2(3) 1];
            pt2 =[seg_bndy2(1) seg_bndy2(4) 1];
        
            % side 2 end pts
            pt3 =[seg_bndy2(2) seg_bndy2(3) 1];
            pt4 =[seg_bndy2(2) seg_bndy2(4) 1];
        
            % transform points
            ptst =  Hopt2*[pt1',pt2',pt3',pt4'];
            ptst(1,:) = ptst(1,:)./ptst(3,:);
            ptst(2,:) = ptst(2,:)./ptst(3,:);
        
            % get side lengths after transformation
            len1 = sqrt(sum((ptst([1 2],1)-ptst([1 2],2)).^2));
            len2 = sqrt(sum((ptst([1 2],3)-ptst([1 2],4)).^2));
            if len1 > len2
                sideratio = len2/len1;
            else
                sideratio = len1/len2;
            end
        
            % check if the ratio is reasonable
            if sideratio>th
                done=1;
                visflag=1;
            else
            
                 % update region
                seg_bndy2(2) = max(seg_bndy2(2)-pixjump,1);
            
                % break if empty
                if seg_bndy2(2)==1
                    seg_bndy2 =[];
                    break;
                end    
            end
    
        end
        
    elseif vpmod==2 || vpmod ==3
      
        done = 0;
        while ~done
        
             % side 1 end pts
            pt1 =[seg_bndy2(1) seg_bndy2(3) 1];
            pt2 =[seg_bndy2(1) seg_bndy2(4) 1];
        
            % side 2 end pts
            pt3 =[seg_bndy2(2) seg_bndy2(3) 1];
            pt4 =[seg_bndy2(2) seg_bndy2(4) 1];
        
            % transform points
            ptst =  Hopt2*[pt1',pt2',pt3',pt4'];
            ptst(1,:) = ptst(1,:)./ptst(3,:);
            ptst(2,:) = ptst(2,:)./ptst(3,:);
        
            % get side ratios after transformation
            len1 = sqrt(sum((ptst([1 2],1)-ptst([1 2],2)).^2));
            len2 = sqrt(sum((ptst([1 2],3)-ptst([1 2],4)).^2));
            if len1 > len2
                sideratio = len2/len1;
            else
                sideratio = len1/len2;
            end
        
            % check if the ratio is reasonable
            if sideratio>th
                done=1;
                visflag=1;
            else
            
                % update region
                seg_bndy2(1) = min(seg_bndy2(1)+pixjump,imcols);
            
                % break if empty
                if seg_bndy2(1)==imcols
                    seg_bndy2 =[];
                    break;
                end    
            end
    
        end
    
    end
    
end



% display the update region
if Do_Display
    h=figure;
    imshow(I); hold on;
    if ~isempty(seg_bndy2)
        plot(seg_bndy2([1 2 2 1 1]),seg_bndy2([3 3 4 4 3]),'r-','LineWidth',4);
    end
    plot(seg_bndy([1 2 2 1 1]),seg_bndy([3 3 4 4 3]),'b-','LineWidth',4);
    close(h);
end

return
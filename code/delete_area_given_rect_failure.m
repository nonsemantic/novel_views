function [seg_bndy2, visflag] = delete_area_given_rect_failure(seg_bndy,gclass)
% delelting some pixels near the vanishing line helps

pixjump = 10;
visflag = 0;
seg_bndy2 = []; % min col, max col, min row, max row 

if strcmp(gclass,'l')
    dum_pt = seg_bndy(2)-pixjump;
    if dum_pt > seg_bndy(1)+pixjump
       seg_bndy2 = seg_bndy;
       seg_bndy2(2) = dum_pt;
       visflag = 1;
    end
elseif strcmp(gclass,'r')
    dum_pt = seg_bndy(1)+pixjump;
    if dum_pt < seg_bndy(2)-pixjump
       seg_bndy2 = seg_bndy;
       seg_bndy2(1) = dum_pt;
       visflag = 1;
    end
elseif strcmp(gclass,'b') 
    dum_pt = seg_bndy(3)+pixjump;
    if dum_pt < seg_bndy(4)-pixjump
       seg_bndy2 = seg_bndy;
       seg_bndy2(3) = dum_pt;
       visflag = 1;
    end
elseif strcmp(gclass,'t')
    dum_pt = seg_bndy(4)-pixjump;
    if dum_pt > seg_bndy(3)+pixjump
       seg_bndy2 = seg_bndy;
       seg_bndy2(4) = dum_pt;
       visflag = 1;
    end
else
    
    dum_bndy = [seg_bndy(1)+pixjump seg_bndy(2)-pixjump  seg_bndy(3)+pixjump seg_bndy(4)-pixjump];
    
    if (dum_bndy(2) > dum_bndy(1)) && (dum_bndy(4) > dum_bndy(3))
       seg_bndy2 = dum_bndy;
       visflag = 1;
    end
    
end
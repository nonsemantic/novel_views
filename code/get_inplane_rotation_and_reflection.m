function [Hopt2,cost2]=get_inplane_rotation_and_reflection(H,line_x,line_y,vpmod,gclass)


Hopt2 = [];
cost2  = 10000000000000;



% transform lines usign metric homography
numpts = size(line_x,1);
pt1 = line_x(:,[1 3]); % end point 1
pt2 = line_x(:,[2 4]); % end point 2

pt1t = (H*[pt1';ones(1,numpts)])'; 
pt1t(:,1) = pt1t(:,1)./pt1t(:,3);
pt1t(:,2) = pt1t(:,2)./pt1t(:,3);

pt2t = (H*[pt2';ones(1,numpts)])';
pt2t(:,1) = pt2t(:,1)./pt2t(:,3);
pt2t(:,2) = pt2t(:,2)./pt2t(:,3);

line_xt = [pt1t(:,1) pt2t(:,1) pt1t(:,2) pt2t(:,2)];

% transform orhtgonal lines usign metric homography
numpts2 = size(line_y,1);
pt1 = line_y(:,[1 3]); pt2 = line_y(:,[2 4]);

pt1t = (H*[pt1';ones(1,numpts2)])';
pt1t(:,1) = pt1t(:,1)./pt1t(:,3);
pt1t(:,2) = pt1t(:,2)./pt1t(:,3);

pt2t = (H*[pt2';ones(1,numpts2)])';
pt2t(:,1) = pt2t(:,1)./pt2t(:,3);
pt2t(:,2) = pt2t(:,2)./pt2t(:,3);

line_yt = [pt1t(:,1) pt2t(:,1) pt1t(:,2) pt2t(:,2)];


% standard directions
xydir = [1 0; ...
         0 1];

% select row depending on vpmode
L_dirchange = [-1 -1; ...
               -1 -1; ...
                1 -1; ...
                1 -1];
            
R_dirchange = [-1  1; ...
               -1  1; ...
                1  1; ...
                1  1];
           
            
C_dirchange = [-1 -1; ...
               -1  1; ...
                1  1; ...
                1 -1];            
            
B_dirchange = [-1  1; ...
                1  1; ...
                1  1; ...
               -1  1];                 
            
T_dirchange = [-1 -1; ...
                1 -1; ...
                1 -1; ...
               -1 -1];                 
            
if strcmp(gclass,'l') % left of the horizon e.g. left wall
      
    % desired directions
    desdir1 = xydir(2,:);
    desdir2 = xydir(1,:);
   
    % current direction
    dir1sign = L_dirchange(vpmod,1);
    dir2sign = L_dirchange(vpmod,2);
    
elseif strcmp(gclass,'r') % right of the horizon e.g. right wall
    
    % desired directions
    desdir1 = xydir(2,:); 
    desdir2 = xydir(1,:);
    
    % current direction
    dir1sign = R_dirchange(vpmod,1);
    dir2sign = R_dirchange(vpmod,2);
  
elseif strcmp(gclass,'c') % center e.g. middle wall
    
    % desired directions
    desdir1 = xydir(2,:); 
    desdir2 = xydir(1,:);
    
    % current direction
    dir1sign = C_dirchange(vpmod,1);
    dir2sign = C_dirchange(vpmod,2);
       
elseif strcmp(gclass,'b') % below horizon e.g. floor
    
     % desired directions
    desdir1 = xydir(1,:); 
    desdir2 = xydir(2,:);
  
    % current direction
    dir1sign = B_dirchange(vpmod,1);
    dir2sign = B_dirchange(vpmod,2);

elseif strcmp(gclass,'t') % above horizon e.g. ceiling
    
     % desired directions
    desdir1 = xydir(1,:); 
    desdir2 = xydir(2,:);
   
    % current direction
    dir1sign = T_dirchange(vpmod,1);
    dir2sign = T_dirchange(vpmod,2);
  
end


 % set opt parameters
 options=optimset( 'Display', 'off', 'MaxIter', 2000, 'TolFun', 10^-200, 'TolX', 10^-10,'MaxFunEvals',10^20, 'LargeScale', 'off');



%% check for four possibilities
homo_param = zeros(1,4);
cost = zeros(1,4);
flipxy_angle = [0     0; ...
                pi    0; ...
                0    pi; ...
                pi  pi];
            
            
flipindx = 1;
[homo_param(flipindx), cost(flipindx)] = ...
                fminunc(@(t)(inplane_and_reflection_homo_opt(t,flipxy_angle(flipindx,1),flipxy_angle(flipindx,2), ...
                                               dir1sign,dir2sign,desdir1,desdir2,line_xt,line_yt)),0, options);
flipindx = 2;
[homo_param(flipindx), cost(flipindx)] = ...
                fminunc(@(t)(inplane_and_reflection_homo_opt(t,flipxy_angle(flipindx,1),flipxy_angle(flipindx,2), ...
                                               dir1sign,dir2sign,desdir1,desdir2,line_xt,line_yt)),0, options);
flipindx = 3;
[homo_param(flipindx), cost(flipindx)] = ...
                fminunc(@(t)(inplane_and_reflection_homo_opt(t,flipxy_angle(flipindx,1),flipxy_angle(flipindx,2), ...
                                               dir1sign,dir2sign,desdir1,desdir2,line_xt,line_yt)),0, options);
flipindx = 4;
[homo_param(flipindx), cost(flipindx)] = ...
                fminunc(@(t)(inplane_and_reflection_homo_opt(t,flipxy_angle(flipindx,1),flipxy_angle(flipindx,2), ...
                                               dir1sign,dir2sign,desdir1,desdir2,line_xt,line_yt)),0, options);
% select the valid inplane homography
[minval,minindx]= min(cost); % only one of the above four can given close to zero error

if length(minindx)~=1
    error('debug me \n');
end

Hopt2 = get_homography_given_param(flipxy_angle(minindx,1),flipxy_angle(minindx,2),1,0,0,homo_param(minindx),1);
Hopt2 = Hopt2*H;
cost2 = minval;

return
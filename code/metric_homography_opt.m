function ang_err = metric_homography_opt(optangles,line_x,line_y,f, cx, cy)

% Zaher et al (2012) "shape from angel regularity"


ang_err =[];
alpha = optangles(1);
beta = optangles(2);


% % camera intrinsics
% cx = 0; cy =0; % assuming image center as image origin
K = [f 0 cx; ...
     0 f cy; ...
     0 0  1];

% 3d rotation matirces 
Rx = [1        0                   0; ...
      0     cos(alpha)   -sin(alpha); ...
      0     sin(alpha)    cos(alpha)];
 
Ry = [ cos(beta)      0     sin(beta); ...
          0           1             0; ... 
      -sin(beta)      0     cos(beta)];

% Rz = [ cos(gamma)   -sin(gamma)      0; ...
%        sin(gamma)    cos(gamma)      0; ...
%          0            0              1];

% get homography matrix 3x3
% H = K Rz Ry Rx  K^-1
% K Rz  is similarity transform. Not needed for metric rectification
H = Ry*Rx*inv(K);
Hinv = inv(H)';
% lines transformed
line_xt =  (Hinv*line_x')'; 
% line_xt_norm = sqrt(line_xt(:,1).^2 + line_xt(:,2).^2);
% line_xt(:,1) = line_xt(:,1)./line_xt_norm;
% line_xt(:,2) = line_xt(:,2)./line_xt_norm;

line_yt =  (Hinv*line_y')';
% line_yt_norm = sqrt(line_yt(:,1).^2 + line_yt(:,2).^2);
% line_yt(:,1) = line_yt(:,1)./line_yt_norm;
% line_yt(:,2) = line_yt(:,2)./line_yt_norm;


% lines normal
normal_xt = line_xt(:,[1 2]);
normal_yt = line_yt(:,[1 2]);

% dot product
dot_xy = normal_xt*normal_yt';

ang_err = sum(abs(dot_xy(:)));


return;
function [part_poly, vispart] =  get_parts_wrt_horizon(vp,vpmod,I,Do_Display)

% input vp 3x2 vanishing points. [vertical vp; horizontal vp; center vp]
%       vpmod 1 out 4 possible vps configuration, i.e., bottom-right-center,
%                                                       bottom-left-center
%                                                       top-left-center  
%                                                       top-right-center 
% This function segments image into 5 parts. left, right, bottom, top,
% center wrt horizon. 
part_poly = zeros(5,4);
vispart = zeros(1,5);
% vispart(5) = 1;
dist = 30;%20; % pixels away from vline

[imrows,imcols,imdims]=size(I);

part_poly(5,:)=[dist imcols-dist dist imrows-dist];

% horizontal vanishing line
vanlineh = cross([vp(2,:) 1], [vp(3,:) 1]);
vanlineh = vanlineh/vanlineh(3); % [a b c], ax+by+c=0

% vertical vanishing line
vanlinev = cross([vp(1,:) 1], [vp(3,:) 1]);
vanlinev = vanlinev/vanlinev(3); % [a b c], ax+by+c=0


% center vanishing line
vanlinec = cross([vp(1,:) 1], [vp(2,:) 1]);
vanlinec = vanlinec/vanlinec(3); % [a b c], ax+by+c=0

% image boundary
imgcr = [1           1; ...
         imcols      1; ...
         imcols imrows; ...
         1      imrows];

% image boundary lines
fcol_line = cross([imgcr(1,:) 1],[imgcr(4,:) 1]);
lcol_line = cross([imgcr(2,:) 1],[imgcr(3,:) 1]);     
frow_line = cross([imgcr(1,:) 1],[imgcr(2,:) 1]);     
lrow_line = cross([imgcr(4,:) 1],[imgcr(3,:) 1]);

partdir = [-10000, 1; ...    % left
            10000, 1; ...    % right
            imrows 10000; ...% bottom
            1     -10000];   % top

partvanline = [vanlinev; ...
               vanlinev; ...
               vanlineh; ...
               vanlineh; ...
               vanlinec];            



for cc=1:5 % left right bottom top
    
    % % check if class exists
    
    % which side of the horizon the current part exist.
    if cc<5
        dirpt = [partdir(cc,:) 1];
        vline = partvanline(cc,:); 
        dirsign = sign(vline*dirpt');
    
        % check if any img bnd exist in this direction
        imgbndsign =  sign(vline* [imgcr';ones(1,4)]);
        crindx = find(imgbndsign==dirsign);
    else
        vline = partvanline(cc,:); 
        imgbndsign =  sign(vline* [imgcr';ones(1,4)]);
        [~,freq]=mode(imgbndsign);
        crindx=1;
        if freq==3
           crindx=[1 1];
        elseif freq==4 
           crindx=[1 1 1 1]; 
        end
    end
    
    rectregion = []; 
    if isempty(crindx) || length(crindx)<2  % rect region not possible
        continue;
    elseif length(crindx)==4 % entire image of one side of vline
        vispart(cc) = 1;       
        if cc==1  %left
             part_poly(cc,:) = [1 imcols-dist 1 imrows];         
        elseif cc==2 % right
             part_poly(cc,:) = [1+dist imcols 1 imrows];
        elseif cc==3 % bottom
             part_poly(cc,:) = [1 imcols 1+dist imrows];
        elseif cc==4  %top
             part_poly(cc,:) = [1 imcols 1 imrows-dist];
        else % center
%              part_poly(5,:)=[dist imcols-dist dist imrows-dist];
               part_poly(5,:)=[1 imcols-1 1 imrows-1];
        end
        
    else  % part of the image on one side of vline
        
        % intersect vline with image bounadires
        p(1,:)=cross(vline,frow_line);%intersection with 1st row of image
        p(2,:)=cross(vline,lrow_line);%intersection with last row of image
        p(3,:)=cross(vline,fcol_line);
        p(4,:)=cross(vline,lcol_line);
        p(:,1) = p(:,1)./p(:,3);
        p(:,2) = p(:,2)./p(:,3);
        p = round(p);
        ind=find(p(:,1) <= imcols & p(:,1)>=1 & p(:,2)<=imrows & p(:,2)>=1);
        if numel(ind)==2
            rectregion =[p(ind(1),[1 2]); ...
                         p(ind(2),[1 2])];
        else
            continue;
        end
        
        
        if cc==1  %left
           dumpt = max(min(rectregion(:,1))-dist,1);
           if dumpt~=1
              vispart(cc) = 1; 
              part_poly(cc,:) = [1 dumpt 1 imrows];
           end
        elseif cc==2 % right
           dumpt = min(max(rectregion(:,1))+dist,imcols);
           if dumpt~=imcols
              vispart(cc) = 1; 
              part_poly(cc,:) = [dumpt imcols 1 imrows];
           end 
        elseif cc==3 % bottom
           dumpt = min(max(rectregion(:,2))+dist,imrows);
           if dumpt~=imrows
              vispart(cc) = 1; 
              part_poly(cc,:) = [1 imcols dumpt imrows];
           end  
        elseif cc==4  %top
          dumpt = max(min(rectregion(:,2))-dist,1);
           if dumpt~=1
              vispart(cc) = 1; 
              part_poly(cc,:) = [1 imcols 1 dumpt];
           end
        else % center
            
            if vpmod==1 || vpmod==4
                dumpt = max(min(rectregion(:,1))-dist,1);
%                 dumpt = max(min(rectregion(:,1))-1,1);
                if dumpt~=1
                    vispart(cc) = 1; 
                    part_poly(cc,:) = [1 dumpt 1 imrows];
                end  
             elseif vpmod==2 || vpmod ==3
                dumpt = min(max(rectregion(:,1))+dist,imcols);
%                 dumpt = min(max(rectregion(:,1))+1,imcols);
                if dumpt~=imcols
                    vispart(cc) = 1; 
                    part_poly(cc,:) = [dumpt imcols 1 imrows];
                end   
%              elseif vpmod==3
%                 dumpt = max(min(rectregion(:,2))-dist,1);
%                 if dumpt~=1
%                     vispart(cc) = 1; 
%                     part_poly(cc,:) = [1 imcols 1 dumpt];
%                 end   
%              else
%                  dumpt = max(min(rectregion(:,2))-dist,1);
%                 if dumpt~=1
%                     vispart(cc) = 1; 
%                     part_poly(cc,:) = [1 imcols 1 dumpt];
%                 end   
             end
            
        end
        
   end
    
end

if Do_Display
    % display vanishing lines
    h1 = figure;
    imshow(I); hold on;
    plot([vp(1,1) vp(3,1)],[vp(1,2) vp(3,2)],'k-','LineWidth',6);
    title('Vertical vanishing line')
    axis tight;
    
    h3 = figure;
    imshow(I); hold on;
    plot([vp(2,1) vp(3,1)],[vp(2,2) vp(3,2)],'k-','LineWidth',6);
    title('horizontal vanishing line')
    axis tight;
    
    h4 = figure;
    imshow(I); hold on;
    plot([vp(1,1) vp(2,1)],[vp(1,2) vp(2,2)],'k-','LineWidth',6);
    title('center vanishing line')
    axis tight;
    
    h2=figure;
    col={'r','c','g','b','y'};
    for cc=1:5
        subplot(2,3,cc);
        imshow(I);hold on;
        plot(part_poly(cc,[1 2 2 1 1]),part_poly(cc,[3 3 4 4 3]),col{cc},'LineWidth',1);       
    end
%     close(h2); close(h1); close(h3); close(h4);  
end

return

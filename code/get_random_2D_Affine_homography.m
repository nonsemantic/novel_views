function Homo = get_random_2D_Affine_homography()


% random initial seed
initial_seed = 2*pi*rand(1,2);
initial_seed = [initial_seed 10*rand(1,1)];


% initial_seed = [0 0 0];
alpha = initial_seed(1);
beta = initial_seed(2);
gamma = initial_seed(3);

% 3d rotation matirces 
Rphi = [cos(alpha)   -sin(alpha); ...
        sin(alpha)    cos(alpha)];
    
Rphi_min = [cos(-alpha)   -sin(-alpha); ...
            sin(-alpha)    cos(-alpha)];
    
 
Rtheta = [cos(beta)   -sin(beta); ...
          sin(beta)    cos(beta)];

D=[gamma 0;
    0    1]; 

% % get homography matrix 3x3
A = Rtheta*Rphi_min*D*Rphi;
t = [0 0]';

Homo = [A   t; ...
        0 0 1];

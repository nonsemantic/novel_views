function [lines1,lines2]=order_line_endpts(lines1,lines2,vps,I,Do_Display)

% order lines endpts away from the vps. x1 y1 closer to vp. direction
% vector pt1-pt1 pts away from vp

% linesx [x1 x2 y1 y2]

numl1 = size(lines1,1);
numl2 = size(lines2,1);

dir1 = zeros(numl1,4); % [direction, st pt]; 
dir2 = zeros(numl2,4);

vp1 = vps(1,:);
vp2 = vps(2,:);

for ll=1:numl1
    
    pt1 = lines1(ll,[1 3]);
    pt2 = lines1(ll,[2 4]);
    
    dist1 = sum((pt1-vp1).^2);
    dist2 = sum((pt2-vp1).^2);
    
    % which line end pt closer to vp
    if dist1 > dist2
%         dumdir = pt1-pt2;
%         dumdir = dumdir./norm(dumdir);
%         dir1(ll,:) = [dumdir pt2]; 
       lines1(ll,:) = [pt2(1) pt1(1) pt2(2) pt1(2)]; % dir vector from pt1 to pt2 i.e., pt2-pt1 is pointing away from vp
%    elseif dist2 > dist1
        
%         dumdir = pt2-pt1;
%         dumdir = dumdir./norm(dumdir);
%         dir1(ll,:) = [dumdir pt1];
%    else
%        error('debug me')
   end
end


for ll=1:numl2
    
    pt1 = lines2(ll,[1 3]);
    pt2 = lines2(ll,[2 4]);
    
    dist1 = sum((pt1-vp2).^2);
    dist2 = sum((pt2-vp2).^2);
    
    % which line end pt closer to vp
    if dist1 > dist2
%         dumdir = pt1-pt2;
%         dumdir = dumdir./norm(dumdir);
%         dir2(ll,:) = [dumdir pt2];
         lines2(ll,:) = [pt2(1) pt1(1) pt2(2) pt1(2)]; % dir vector from pt1 to pt2 i.e., pt2-pt1 is pointing away from vp
             
%     elseif dist2 > dist1
% %         dumdir = pt2-pt1;
% %         dumdir = dumdir./norm(dumdir);
% %         dir2(ll,:) = [dumdir pt1];
%          
%     else
%         error('debug me')
    end
end


% % display the direction vectors
% if Do_Display
%     
%     h=figure;
%     imshow(I); hold on;
%     scale=50;
%     quiver(dir1(:,3),dir1(:,4),scale*dir1(:,1),scale*dir1(:,2),'r');
%     quiver(dir2(:,3),dir2(:,4),scale*dir2(:,1),scale*dir2(:,2),'b');
%     close(h)
% end



return;
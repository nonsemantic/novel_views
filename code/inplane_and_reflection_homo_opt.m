function res = inplane_and_reflection_homo_opt(optangles,alpha,beta,dir1sign,dir2sign,desdir1,desdir2,line_x,line_y)


res =[];

gamma = optangles;


% 3d rotation matirces 
Rx = [1        0                   0; ...
      0     cos(alpha)   -sin(alpha); ...
      0     sin(alpha)    cos(alpha)];
 
Ry = [ cos(beta)      0     sin(beta); ...
          0           1             0; ... 
      -sin(beta)      0     cos(beta)];

Rz = [ cos(gamma)   -sin(gamma)      0; ...
       sin(gamma)    cos(gamma)      0; ...
         0            0              1];

% get homography matrix 3x3
% H = Rz Ry Rx 
H = Rz*Ry*Rx;

% transform lines usign inplane+reflect homography
numpts = size(line_x,1);
pt1 = line_x(:,[1 3]); % end point 1
pt2 = line_x(:,[2 4]); % end point 2

pt1t = (H*[pt1';ones(1,numpts)])'; 
pt1t(:,1) = pt1t(:,1)./pt1t(:,3);
pt1t(:,2) = pt1t(:,2)./pt1t(:,3);

pt2t = (H*[pt2';ones(1,numpts)])';
pt2t(:,1) = pt2t(:,1)./pt2t(:,3);
pt2t(:,2) = pt2t(:,2)./pt2t(:,3);

% get unit direction vector
dir1 = pt2t(:,[1 2])-pt1t(:,[1 2]);
normdir1 = sqrt(sum(dir1(:,1).^2 + dir1(:,2).^2,2));
dir1(:,1) = dir1(:,1)./normdir1;
dir1(:,2) = dir1(:,2)./normdir1;
dir1 = dir1sign*dir1; % depends on vpmod

% desired direction 1
desdir1 = repmat(desdir1,numpts,1); 
    
% transform orhtgonal lines usign inplane+reflect homography
numpts2 = size(line_y,1);
pt1 = line_y(:,[1 3]); pt2 = line_y(:,[2 4]);

pt1t = (H*[pt1';ones(1,numpts2)])';
pt1t(:,1) = pt1t(:,1)./pt1t(:,3);
pt1t(:,2) = pt1t(:,2)./pt1t(:,3);

pt2t = (H*[pt2';ones(1,numpts2)])';
pt2t(:,1) = pt2t(:,1)./pt2t(:,3);
pt2t(:,2) = pt2t(:,2)./pt2t(:,3);


% get unit direction vector
dir2 = pt2t(:,[1 2])-pt1t(:,[1 2]);
normdir2 = sqrt(sum(dir2(:,1).^2 + dir2(:,2).^2,2));
dir2(:,1) = dir2(:,1)./normdir2;
dir2(:,2) = dir2(:,2)./normdir2;

dir2 = dir2sign*dir2;% depends on vpmod

% desired derection 2
desdir2 = repmat(desdir2,numpts2,1);

% check if directions aligned. If lines parallel, dot product of their direction unit vectors must be 1.
res = sum(sum(abs(1-dir1*desdir1')))+ sum(sum(abs(1-dir2*desdir2')));  % res =1-cos(theta) , if theta = 0 (line parallel), res = 0

return
function Homo = get_random_homography(cx,cy,f)


% % camera intrinsics
% cx = 0; cy =0; % assuming image center as image origin
K = [f 0 cx; ...
     0 f cy; ...
     0 0  1];

% random initial seed
initial_seed = (pi/45)*rand(1,3);

% initial_seed = [0 0 0];
alpha = initial_seed(1);
beta = initial_seed(2);
gamma = initial_seed(3);

% 3d rotation matirces 
Rx = [1        0                   0; ...
      0     cos(alpha)   -sin(alpha); ...
      0     sin(alpha)    cos(alpha)];
 
Ry = [ cos(beta)      0     sin(beta); ...
          0           1             0; ... 
      -sin(beta)      0     cos(beta)];

Rz = [ cos(gamma)   -sin(gamma)      0; ...
       sin(gamma)    cos(gamma)      0; ...
         0            0              1];

% % get homography matrix 3x3
Homo = K*Rz*Ry*Rx*inv(K);

% % get homography matrix 3x3
% Homo = Rz*Ry*Rx;
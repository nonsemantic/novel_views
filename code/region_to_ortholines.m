function [lines_vp1,lines_vp2] = region_to_ortholines(seg_bndy,vp,nsamp,img,Do_display)

% Input
% seg_bndy patch boundary [min_x max_x min_y max_y]
% vp 2x2, vanishing points defining segment orientation

% Output
% lines_vp1, Nx4, N lines segments [x1 x2 y1 y2]
% lines_vp2, Mx4, M lines segments 

lines_vp1 = [];
lines_vp2 = [];

[h,w,d] = size(img);

min_x = seg_bndy(1); max_x=seg_bndy(2); min_y=seg_bndy(3); max_y=seg_bndy(4);

rays = cell(1,2);
for vno = 1:2 % get rays
    
    ll1 = [vp(vno,1) seg_bndy(1) vp(vno,2) seg_bndy(3)]; % x1 x2 y1 y2
    ll2 = [vp(vno,1) seg_bndy(1) vp(vno,2) seg_bndy(4)];
    ll3 = [vp(vno,1) seg_bndy(2) vp(vno,2) seg_bndy(3)];
    ll4 = [vp(vno,1) seg_bndy(2) vp(vno,2) seg_bndy(4)];
    
    lla = [ll1; ll1; ll1; ll2; ll2; ll3];
    llb = [ll2; ll3; ll4; ll3; ll4; ll4];
    
    veca = lla(:,[2,4])-lla(:,[1,3]);
    vecb = llb(:,[2,4])-llb(:,[1,3]);
    norma=(sum(veca.*veca,2)).^.5;
    normb=(sum(vecb.*vecb,2)).^.5;
    theta = acosd(dot(veca,vecb,2)./norma./normb);
    [vv ii]=max(theta);
    
%     rays{1,vno} =  [lla(ii,:); ...
%                     llb(ii,:)];
    
%     st_ang1  =wrapTo360(atan2d(lla(ii,4)-lla(ii,3), ...
%                                lla(ii,2)-lla(ii,1)));
%     
%     end_ang2 =wrapTo360(atan2d(llb(ii,4)-llb(ii,3), ...
%                                llb(ii,2)-llb(ii,1)));
                
%     for ang=st_ang1:(end_ang2-st_ang1)/nsamp:end_ang2
   dtheta=theta(ii)/nsamp;   
   for ang=dtheta:dtheta:180
        pa=[];
        pb=[];
        m=tand(ang);
        
        if isinf(m)
%             pa=[vp(vno,1) h];
%             pb=[vp(vno,1) 1];
            continue;
        elseif m==0
%             pa=[1 vp(vno,2)];
%             pb=[w vp(vno,2)]; 
            continue;
        else
%             c=vp(vno,2)-m*vp(vno,1);
%             x=(h-c)/m;
%             p(1,:)=[x h];%intersection with last row of image
%             x=(1-c)/m;
%             p(2,:)=[x 1];%intersection with 1st row of image
%             y=m*w+c;
%             p(3,:)=[w y];
%             y=m*1+c;
%             p(4,:)=[1 y];
%             
%             ind=find(p(:,1) <= w & p(:,1)>=1 & p(:,2)<=h & p(:,2)>=1);

            c=vp(vno,2)-m*vp(vno,1);
            x=(max_y-c)/m;
            p(1,:)=[x max_y];%intersection with last row of image
            x=(min_y-c)/m;
            p(2,:)=[x min_y];%intersection with 1st row of image
            y=m*max_x+c;
            p(3,:)=[max_x y];
            y=m*min_x+c;
            p(4,:)=[min_x y];
            
            ind=find(p(:,1) <= max_x & p(:,1)>=min_x & p(:,2)<=max_y & p(:,2)>=min_y);
%                 ind=find(p(:,1)< max_x & p(:,1)>min_x & p(:,2)<max_y & p(:,2)>min_y);
            if numel(ind)==2
                pa=p(ind(1),:);
                pb=p(ind(2),:);
                
                if sum(pa-pb)==0 % get rid of lines with zero length
                    continue;
                end
            end
        end
        
        if numel(pa) ==2 && numel(pb)==2
            rays{1,vno}=[rays{1,vno};pa(1) pb(1) pa(2) pb(2)]; %lines=[x1 x2 y1 y2];
            
        end
    end
    
   
    
end


lines_vp1 = rays{1,1};
lines_vp2 = rays{1,2};
    
    
if Do_display
   figure(124);
   imshow(img); hold on;
   plot(lines_vp1(:,1:2)', lines_vp1(:,3:4)','r');
   plot(lines_vp2(:,1:2)', lines_vp2(:,3:4)','b');
end

end




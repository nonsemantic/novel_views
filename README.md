1. Demo script: demo_single_img_rectification_multiparts.m

2. Few directories are created in the base folder to save the results. Make sure the base directory has write permission.

--- ./rectifiedimgs folder contains the resulting images.
--- ./rectification_data folder contains the rectification homography.
--- ./rectifiedmasks folder contains the image masks.

3. A given image is converted into five frontal views, i.e, left, right, top, bottom and center. 

--- Each above mentioned part can be subdivided into multiple parts for reasonable resolution. See Fig 3a in [1].

--- The saved data nomenclature:  rectified image:               imagename_partnumber_subpartnumber_angle.ext is saved in ./rectifiedimgs
                                  rectification homography data: imagename_partnumber_subpartnumber_angle.mat is saved in ./rectification_data
    
    NOTE: not all images has all the five parts. Absence of any part may be due to this reason or may be the rectification failed for this part.

--- Few flag files:  ./rectifiedimgs/imagename_rectdone.txt means image has been processed irrespective of the outcome.
                     
                     ./rectifiedimgs/imagename_vpfail.txt means that the vanishing point estimation failed and the process aborted.
                     
                     ./rectifiedimgs/imagename_partnumber_subpartnumber_angle.txt means that the rectified image has crazy dimensions (few rows or few columans). Not useful for feature extraction.

                     ./rectifiedimgs/imagename_rectfail.txt means that the rectification failed although VP estimation was OK.


------------------ Unsupervised Discriminative Invariant Mid-Level Patches ---------------------



4. For this task we will use the unsupervised mid-level patch discovery code provided by Singh et al. [4] at the following link:

   https://github.com/saurabhme/discriminative-patches

   Instead of using normal perspective images, we will use the output of steps 1-3. Be patient and read the documentation of this code for preparing the input file.
   We have done few modifications to this code, that I will explain step by step:


  4a: The starting file is ../user/trainDiscPats.m. 
      Line 6-13 shows how to prepare the input data file in pascal format.
      Line 22: warpCrossValClusteringUnsup(1, USR, true) is the main function for patch discovery. We will modify it. Rest remains the same.

  4b: Modifying  ../warpscripts/warpCrossValClusteringUnsup.m.
      
     -- Comment line 111-113.

     -- Add the following code after line 113: 

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    initfeat_flag = 1;
    
    % sample 3 times to have some patches  
    for dups=1:3
 
                 [positivePatchesD, posFeaturesD, posCorrespIndsD] = ...
                    getRandomPatchesFromPyramid(trainAllPos(trainSetPos), params, ...
                    params.homeImgs.pos);

                %filter out patches not on the mask
                valid = find(filterPatchList(positivePatchesD) > 0.995);
                positivePatchesD = positivePatchesD(valid); 
                posFeaturesD = posFeaturesD(valid,:); 
                posCorrespIndsD = posCorrespIndsD(valid);
                fprintf('val features = %d \n', numel(posCorrespIndsD))
                %continue as usual

                if initfeat_flag == 1
                    initfeat_flag = 0; 
                    positivePatches = positivePatchesD;
                    posFeatures = posFeaturesD;
                    posCorrespInds = posCorrespIndsD;
                else
                    positivePatches = [positivePatches; positivePatchesD];
                    posFeatures = [posFeatures; posFeaturesD];
                    posCorrespInds = [posCorrespInds; posCorrespIndsD];
                end
    end
   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                                                   
 4c: In the above code there is this external function filterPatchList.m. 

     Line 9 and Line 31: maskName = [strrep(fpath,'Images','masks') '/' name '.png'];
                         This code assumes that the masks corresponding the images (see step 2)are availble at a certain location.
                         Modify this line to point towards the path with the homography mask corresponding to the images are placed.
                         Do this only for positive images.  
                                                                               


These modifications are added to the file "../warpscripts/warpCrossValClusteringUnsup.m" and the new function "warpCrossValClusteringUnsup2.m" is available in the folder ../code_patches.

5. Contact:

For any questions, I can be reached via email at wajih.1234@gmail.com

References:

1. Wajahat hussain, Javier Civera, Luis Montano, Martial Hebert, "Dealing with small data and training blind spots in the Manhattan world", WACV 2016.

2. David F Fouhey, Wajahat Hussain, Abhinav Gupta, Martial Hebert, "Single Image 3D With No 3D Single Image", ICCV 2015.

3. Versha Hedau, Derek Hoiem, David Forsyth, "Recovering the spatial layout of cluttered rooms", ICCV 2009.

4. S. Singh, A. Gupta and A. A. Efros, "Unsupervised discovery of mid-level discriminative patches", ECCV 2012.
function [vps, f]=calibrate_cam_fouhey(vps,h,w)

%     vp = getVPHedauRaw(img);
%     vps = {[vp(1), vp(2)],[vp(3),vp(4)],[vp(5),vp(6)]};
% 
%     w = size(img,2); h = size(img,1);
    fMax = hypot(w,h);

    getOrtho = @(N2)(sum(abs([rad2deg(acos(N2(1,:)*N2(2,:)')), rad2deg(acos(N2(2,:)*N2(3,:)')), rad2deg(acos(N2(1,:)*N2(3,:)'))]-90)));
    getNormals = @(fx)(computeNormalEstimates(vps,fx,w,h));
    getNormalQual = @(fx)(getOrtho(getNormals(fx)));

    fadjustTol = optimset('TolX',1e-6);
    f = fminbnd(getNormalQual,1,fMax*30,fadjustTol);
end


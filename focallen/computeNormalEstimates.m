function normals = computeNormalEstimates(vps, f, w, h)
    %David Fouhey, Abhinav Gupta, Martial Hebert
    %Data-Driven 3D Primitives For Single Image Understanding 
    %ICCV 2013
    %
    %Inference-only code

    %Mathematical solution from:
    %   Improved orientation estimation for texture planes using multiple vanishing points
    %   Eraldo Ribeiro1, Edwin R. Hancock
    %   Pattern Recognition 33 (2000) 1599}1610

    %Given the vanishing points, focal length, and image width,
    %compute the surface normals for the vanishing points.
    %Assumes center principal point.
    normals = [];
    cx = w / 2; cy = h / 2;

    %for each vanishing point
    for i=1:3
        otherIds = setdiff(1:3,[i]);
        vp1 = vps{otherIds(1)}; vp2 = vps{otherIds(2)};
        x1 = vp1(1); y1 = vp1(2); x2 = vp2(1); y2 = vp2(2);
        %center the coordinates
        x1 = x1 - cx; x2 = x2 - cx;
        y1 = y1 - cy; y2 = y2 - cy;
       
        %get the vector
        p = f*(y1 - y2)/(x1*y2 - x2*y1);
        q = f*(x2 - x1)/(x1*y2 - x2*y1);
        normals = [normals; p, q, 1];
    end
    %normalize
    normDiv = sum(normals.^2,2).^0.5;
    normals = normals ./ repmat(normDiv,1,3);
   
    %figure out underflow / overflows
    unestimated = find(sum(isnan(normals),2));
    if numel(unestimated) == 1
        %if we have one undetermined, it's just the cross product
        %this occurs due to numerical issues
        estimated = setdiff(1:3,[unestimated]);
        normals(unestimated,:) = cross(normals(estimated(1),:),normals(estimated(2),:));
    elseif numel(unestimated) == 2
        %if we have two, then something's seriously funny 
        %but we can just use any basis for the nullspace of the one normal we know
        estimated = setdiff(1:3,[unestimated]);
        normals(unestimated,:) = null(normals(estimated(1),:))';
    elseif numel(unestimated) == 3
        %for completeness.
        normals = eye(3); 
    end
end

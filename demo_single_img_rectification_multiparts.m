% This function rectifies an image into at most five part w.r.t horizon
% Five rectified parts left, right, top, bottom, center w.r.t horizon are
% stored in savedir, i.e., rectifiedimgs
% Author: wajahat hussain, 5 July 2014

close all;
clear;
clc;

% add paths
addpath('./code'); 
addpath('./ComputeVP');
addpath('./focallen');
% addpath('./utils');

% images dir
src = './dataset/';

% datadir (e.g. vanishing points)
datadir = './data/';
if ~exist(datadir,'dir')
    mkdir(datadir)
end

% output dir (contains rectified version)
savedir = './rectifiedimgs/';
if ~exist(savedir,'dir')
    mkdir(savedir)
end

% output dir (contains rectified masks)
maskdir = './rectifiedmasks/';
if ~exist(maskdir,'dir')
    mkdir(maskdir)
end

% output dir (contains rectification data)
homodir = './rectification_data/';
if ~exist(homodir,'dir')
    mkdir(homodir)
end

% enter the image name
imgname = '131011c.jpg'; %'02_16_resized.jpg';%'131011c.jpg'; % see src for sample images


parts_flag = 1;  % 1: if subdividing each scene w.r.t horizon into mutlple parts 0: otherwise
partmode  = 2;   % if parts_flag 1: 1: invarinat patches 2: SVT Text
num_parts = 2;   % if parts_flag 1:    num_parts >=2

% run on a single image
tic
% try 
%    complete_metric_rectification_multiparts(src,imgname,datadir,savedir,maskdir,homodir)
     complete_metric_rectification_multiparts2(src,imgname,datadir,savedir,maskdir,homodir,parts_flag,partmode,num_parts);
% catch
% end
toc


% Note: if the above code ran correctly there is a rectified image
% 131011c_left_1.jpg in savedir. We can related image portions between rectified
% and normal image using the following code

% get perspective image
img = imread(fullfile(src,imgname));

% get  one part of the scene
partname = '_left_1';

% get rectified image
rimgname = fullfile(savedir,[imgname(1:end-4) partname '.jpg']);
rimg =imread(rimgname);

% get rectification homography
homofile = fullfile(homodir,[imgname(1:end-4) partname '.mat']);
load(homofile,'Homo_rect','newT');
% pt2 = inv(Homo_rect)*newT*pt1
% pt2 is in unrectified image
% pt1 in rectified image


% selected quadrilateral on the rectified image
h1=figure;
imshow(img);

h2 = figure;
imshow(rimg); hold on;
% [x,y] = ginput(4);
x = [24 41 43 27]'; 
y = [113 113 132 133]';

plot([x' x(1)],[y' y(1)],'r-','LineWidth',4);

% tranfer quadrilateral on the normal image
xyt=inv(Homo_rect)*newT*[x'; y'; ones(1,4)];
xyt(1,:) = xyt(1,:)./xyt(3,:);
xyt(2,:) = xyt(2,:)./xyt(3,:);
figure(h1);
hold on;
plot([xyt(1,:) xyt(1,1)],[xyt(2,:) xyt(2,1)],'r-','Linewidth',4);



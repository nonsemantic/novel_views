%Copyright (c) October,15 2008 by Varsha Hedau, UIUC.  All rights reserved.
function Xpnts2 = RemoveRedundantPoints3(Xpnts,w,h, Do_Display)

%%%%% remove redundant points %%%%%%
currid=1;
done=0;
Xpnts2=Xpnts;
numpts = size(Xpnts,1);

if Do_Display
    h = waitbar(0,'Initializing waitbar...');
end

while size(Xpnts,1)>0
    Xpnts2(currid,:) = Xpnts(1,:);
   
    currid=currid+1;
    dists = (Xpnts(1,1)-Xpnts(:,1)).^2 + ...
        (Xpnts(1,2)-Xpnts(:,2)).^2;
    if sqrt((Xpnts(1,1)-w)^2+(Xpnts(1,2)-h)^2)/sqrt((w/2)^2+(h/2)^2) < 1
        thres=150;%10;
    else
%         thres=20*(sqrt((Xpnts(1,1)-w)^2+(Xpnts(1,2)-h)^2)/sqrt((w/2)^2+(h/2)^2));
        thres=500*(sqrt((Xpnts(1,1)-w)^2+(Xpnts(1,2)-h)^2)/sqrt((w/2)^2+(h/2)^2));
    end
    inds=find(dists>thres);
    Xpnts = Xpnts(inds,:);
    
    if Do_Display
       waitbar(size(Xpnts,1)/numpts,h,sprintf('%f%% to go...',size(Xpnts,1)/numpts)) 
    end
end
Xpnts2 = Xpnts2(1:currid-1,:);

if Do_Display
       close(h);
end

return;